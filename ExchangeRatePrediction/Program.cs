﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessTier;
using DataTier.Models;

namespace ExchangeRatePrediction
{
    class Program
    {
        static void Main(string[] args)
        {
            BusinessLogicLayer businessLogicLayer = new BusinessLogicLayer();

            List<string> rangeOfValid = businessLogicLayer.GetAvailableCurrenciesOpenExchange().Keys.ToList();

            Console.WriteLine("EXCHANGE RATE PREDICTION PROGRAM");

            Console.WriteLine("Please input the base currency: ");
            string inputBaseCurrency = Console.ReadLine();
            bool isCheck = true;
            bool isEmpty = String.IsNullOrEmpty(inputBaseCurrency);
            bool isIndexOfRange = rangeOfValid.Contains(inputBaseCurrency?.ToUpper());

            if (isEmpty && isIndexOfRange)
            {
                isCheck = false;
            }

            while (isCheck)
            {
                bool isEmptyInLoop = String.IsNullOrEmpty(inputBaseCurrency);
                bool isIndexOfRangeInLoop = rangeOfValid.Contains(inputBaseCurrency?.ToUpper());

                if (isEmptyInLoop)
                {
                    Console.WriteLine("The base currency can not be empty, please input: ");
                    inputBaseCurrency = Console.ReadLine();
                } else
                if (!isIndexOfRangeInLoop)
                {
                    Console.WriteLine("The currency is invalid, please input the another base currency: ");
                    inputBaseCurrency = Console.ReadLine();
                }

                if (!isEmptyInLoop && isIndexOfRangeInLoop)
                {
                    isCheck = false;
                }
            }

            Console.WriteLine("Please input compared currency: ");
            string inputComparedCurrency = Console.ReadLine();
            isCheck = true;
            isEmpty = String.IsNullOrEmpty(inputComparedCurrency);
            isIndexOfRange = rangeOfValid.Contains(inputComparedCurrency?.ToUpper());

            if (isEmpty && isIndexOfRange)
            {
                isCheck = false;
            }

            while (isCheck)
            {
                bool isEmptyInLoop = String.IsNullOrEmpty(inputComparedCurrency);
                bool isIndexOfRangeInLoop = rangeOfValid.Contains(inputComparedCurrency?.ToUpper());

                if (isEmptyInLoop)
                {
                    Console.WriteLine("The compared currency can not be empty, please input: ");
                    inputComparedCurrency = Console.ReadLine();
                }
                else
                if (!isIndexOfRangeInLoop)
                {
                    Console.WriteLine("The currency is invalid, please input the another compared currency: ");
                    inputComparedCurrency = Console.ReadLine();
                }

                if (!isEmptyInLoop && isIndexOfRangeInLoop)
                {
                    isCheck = false;
                }
            }

            Console.WriteLine("Calculating...");

            BusinessEntity businessEntity = new BusinessEntity();
            businessEntity.InputDate = "2017-01-15";
            businessEntity.BaseCurrency = inputBaseCurrency?.ToUpper();
            businessEntity.ComparedCurrency = inputComparedCurrency?.ToUpper();

            double predictedRate = businessLogicLayer.CalculateExchangeRate(businessEntity);

            Console.WriteLine("The predicted currency exchange from {0} to {1} for {2} is {3}", inputBaseCurrency?.ToUpper(), inputComparedCurrency?.ToUpper(), "2017-01-15", predictedRate.ToString("0." + new string('#', 339)));

            Console.ReadLine();

        }
    }
}
