﻿using System;
using BusinessTier;
using DataTier.Models;
using NUnit.Framework;

namespace UnitTest
{
    [TestFixture]
    class BusinessTierUnitTest
    {
        [Test]
        public void Test_Calculate_Exchange_Rate()
        {
            BusinessEntity entity = new BusinessEntity();
            entity.InputDate = "2018-12-24";
            entity.BaseCurrency = "USD";
            entity.ComparedCurrency = "VND";

            RegressionEquation regressionEquation = new RegressionEquation();
            regressionEquation.BusinessEntity = entity;

            double result = regressionEquation.CalculateExchangeRate();
            Assert.AreEqual(Math.Round(22730.0759782179000, 8, MidpointRounding.AwayFromZero), Math.Round(result, 8, MidpointRounding.AwayFromZero));
        }
    }
}
