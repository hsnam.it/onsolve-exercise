﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Xml;
using System.Xml.Linq;
using DataTier.DataHandlerImplement;
using DataTier.Models;
using NUnit.Framework;
using XmlReader = DataTier.XmlReader;

namespace UnitTest
{
    [TestFixture]
    public class DataTierUnitTest
    {
        [Test]
        public void Test_Read_Config_File_At_The_Same_Folder_Of_Executed_Dll()
        {
            //Make sure you put the file into test folder
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ServiceConfig.xml");
            XElement xmlElement = XmlReader.XmlFileToXElement(filePath);
            Assert.IsNotEmpty(xmlElement.Element("Source").Value); 
        }

        [Test]
        public void Test_Read_AppId_From_ConfigFile()
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ServiceConfig.xml");
            XElement xmlData = XmlReader.XmlFileToXElement(filePath);
            var appId = xmlData.Elements("Source")
                .Where(source => source.Element("Name").Value.Equals("OpenExchangeAPI"))
                .Select(source => source.Element("AppId").Value);
            Assert.AreEqual("aecd3058ec27405283459f1867c3c8fe", appId.ElementAt(0));
        }

        [Test]
        public void Test_GetAvailableCurrencies_20181224()
        {
            OpenExchangeRepository openExchangeRepository = new OpenExchangeRepository();
            Dictionary<string, string> dictCurrencies = openExchangeRepository.GetAvailableCurrencies();
            Assert.AreEqual(171, dictCurrencies.Count);
        }
    }
}
