﻿namespace BusinessTier.Interfaces
{
    public interface IAlgorithm
    {
        double CalculateExchangeRate();
    }
}
