﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessTier.Interfaces;
using DataTier;
using DataTier.Models;

namespace BusinessTier
{
    public class RegressionEquation : IAlgorithm
    {
        public BusinessEntity BusinessEntity { get; set; }

        private const string DateFormat = "yyyy-MM-dd";

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double CalculateExchangeRate()
        {
            List<ExchangeRateData> lstSampleData = GetSampleData(BusinessEntity);

            //X: month, Y: ExchangeRate
            //Find ΣX, ΣY, ΣXY, ΣX2.

            int itemCount = lstSampleData.Count;

            //ΣX
            double sumX = 0;
            //ΣY
            double sumY = lstSampleData.Sum(record => record.ExchangeRate);
            //ΣXY
            double sumXy = 0;
            //ΣX2
            double sumX2 = 0;
            for (int i = 0; i < itemCount; i++)
            {
                sumX += (i + 1);
                sumXy += (i + 1) * lstSampleData[i].ExchangeRate;
                sumX2 += Math.Pow((i + 1), 2);
            }

            //Slope(b) = (NΣXY - (ΣX)(ΣY)) / (NΣX2 - (ΣX)2)
            double slope = ((itemCount * sumXy) - (sumX * sumY)) / ((itemCount * sumX2) - Math.Pow(sumX, 2));

            //Intercept(a) = (ΣY - b(ΣX)) / N 
            double intercept = (sumY - (slope * sumX)) / itemCount;

            //Regression Equation(y) = a + bx 
            double predictedExchangeRate = intercept + slope * DateTime.ParseExact(BusinessEntity.InputDate, DateFormat, null).Month;

            return predictedExchangeRate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private List<ExchangeRateData> GetSampleData(BusinessEntity entity)
        {
            List<ExchangeRateData> lstResult = new List<ExchangeRateData>();
            DbContext dbContext = new DbContext();
            
            DateTime inputDate = DateTime.ParseExact(entity.InputDate, DateFormat, null);
            DateTime startDate = new DateTime(inputDate.Year - 1, 1, inputDate.Day);
            DateTime endDate = startDate.AddMonths(11);

            while (startDate <= endDate)
            {
                entity.InputDate = startDate.ToString(DateFormat);
                lstResult.Add(dbContext.GetOpenExchangeRepo().GetData(entity));
                startDate = startDate.AddMonths(1);
            }

            return lstResult;
        }
    }
}
