﻿using System.Collections.Generic;
using BusinessTier.Interfaces;
using DataTier;
using DataTier.DataHandlerImplement;
using DataTier.Interfaces;
using DataTier.Models;

namespace BusinessTier
{
    public class BusinessLogicLayer
    {
        /// <summary>
        /// Calculate the predicted exchange rate for the given currencies
        /// </summary>
        /// <param name="businessEntity"></param>
        /// <returns> The predicted exchange rate : double</returns>
        public double CalculateExchangeRate(BusinessEntity businessEntity)
        {
            RegressionEquation regressionEquation = new RegressionEquation();
            regressionEquation.BusinessEntity = businessEntity;
            double predictedRate = regressionEquation.CalculateExchangeRate();
            return predictedRate;
        }
        
        /// <summary>
        /// Return available currencies of Open Exchange API
        /// </summary>
        /// <returns> Code and Currency Name </returns>
        public Dictionary<string, string> GetAvailableCurrenciesOpenExchange()
        {
            DbContext dbContext = new DbContext();
            Dictionary<string, string> dictAvailableCurrencies = dbContext.GetOpenExchangeRepo().GetAvailableCurrencies();

            return dictAvailableCurrencies;
        }
    }
}
