﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Linq;
using DataTier.Interfaces;
using DataTier.Models;

namespace DataTier.DataHandlerImplement
{
    public class OpenExchangeRepository : IRepository
    {
        private string _appId;
        private const string DataSource = "https://openexchangerates.org/api";
        private const string DefaultCurrency = "USD";
        private const string DataSourceConfigFileName = "ServiceConfig.xml";

        public ExchangeRateData GetData(BusinessEntity entity)
        {
            try
            {
                if (String.IsNullOrEmpty(_appId))
                {
                    _appId = getAppId();
                }

                string urlRequest;
                if (DefaultCurrency.Equals(entity.BaseCurrency))
                {
                    urlRequest = DataSource + "/historical/" + entity.InputDate + ".json?app_id=" + _appId;
                }
                else
                {
                    //TODO: if you have the right to change the base currency, change this urlRequest
                    urlRequest = DataSource + "/historical/" + entity.InputDate + ".json?app_id=" + _appId;
                }

                Task<string> callApiTask = RequestApiAsync(urlRequest);
                IQueriedJsonRecord queriedJsonRecord = new OpenExchangeJsonRecord();

                string respondedBody = callApiTask.Result;
                if (String.IsNullOrEmpty(respondedBody))
                    return null;

                queriedJsonRecord.GetDataFromResponseBody(callApiTask.Result);
                queriedJsonRecord = (OpenExchangeJsonRecord)queriedJsonRecord;

                ExchangeRateData exchangeRateData;
                if (DefaultCurrency.Equals(entity.BaseCurrency))
                {
                    exchangeRateData = new ExchangeRateData(
                        entity.BaseCurrency,
                        entity.ComparedCurrency,
                        ((OpenExchangeJsonRecord)queriedJsonRecord).Rates[entity.ComparedCurrency]
                    );
                }
                else
                {
                    //TODO: if you have the right to change the base currency, remove this code block
                    exchangeRateData = new ExchangeRateData(
                        entity.BaseCurrency,
                        entity.ComparedCurrency,
                         1 / ((OpenExchangeJsonRecord)queriedJsonRecord).Rates[entity.BaseCurrency]
                    );
                }

                return exchangeRateData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpRequestException();
            }
        }

        /// <summary>
        /// Get all supported currencies of OpenExchangeRates API
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetAvailableCurrencies()
        {
            try
            {
                string urlRequest = DataSource + "/currencies.json";

                Task<string> callApiTask = RequestApiAsync(urlRequest);
                if (String.IsNullOrEmpty(urlRequest))
                {
                    Console.WriteLine("Url request to Open Exchange API is empty!");
                    return null;
                }

                string respondedBody = callApiTask.Result;
                if (String.IsNullOrEmpty(respondedBody))
                    return null;

                IQueriedJsonRecord queriedJsonRecord = new AvailableCurrenciesJsonRecord();
                queriedJsonRecord.GetDataFromResponseBody(callApiTask.Result);
                return ((AvailableCurrenciesJsonRecord)queriedJsonRecord).AvailableCurrencies;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlRequest"></param>
        /// <returns></returns>
        private async Task<string> RequestApiAsync(string urlRequest)
        {               
            using (var client = new HttpClient())
            {
                try
                {
                    string respondedBody = await client.GetStringAsync(urlRequest);
                    return respondedBody;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Getting Open Exchange data: "+ e.Message);
                    throw new HttpRequestException();
                }
            }
        }

        /// <summary>
        /// Get AppId of OpenExchangeApi from ServiceConfig.xml
        /// </summary>
        /// <returns></returns>
        private string getAppId()
        {
            try
            {
                XElement xmlData = XmlReader.XmlFileToXElement(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DataSourceConfigFileName));
                var appId = xmlData.Elements("Source")
                    .Where(source => source.Element("Name").Value.Equals("OpenExchangeAPI"))
                    .Select(source => source.Element("AppId").Value);
                return appId.ElementAt(0);
            }
            catch (Exception)
            {
                Console.WriteLine("Fail to load ServiceConfig.xml, the file maybe broken for wrong format!");
                throw;
            }
        }
    }
}
