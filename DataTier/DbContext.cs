﻿using System;
using DataTier.DataHandlerImplement;
using DataTier.Interfaces;

namespace DataTier
{
    public class DbContext
    {
        //Add new repositories here
        private IRepository _openExchangeRepository;

        public IRepository GetOpenExchangeRepo()
        {
            if (_openExchangeRepository == null)
                _openExchangeRepository = new OpenExchangeRepository();
            return _openExchangeRepository;
        }
    }
}
