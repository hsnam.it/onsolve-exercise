﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataTier
{
    public class XmlReader
    {
        public static XElement XmlFileToXElement(string filePath)
        {
            if (String.IsNullOrEmpty(filePath))
            {
                throw new FileLoadException("The file path is empty");
            }

            try
            {
                XElement xmlData = XElement.Load($"{filePath}");
                return xmlData;

            }
            catch (Exception)
            {
                throw new FileLoadException("Fail to load Xml file.");
            }
        }
    }
}
