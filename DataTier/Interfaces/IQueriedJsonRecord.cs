﻿using System;

namespace DataTier.Interfaces
{
    public interface IQueriedJsonRecord
    {
        void GetDataFromResponseBody(string responseBody);
    }
}
