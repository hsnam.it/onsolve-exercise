﻿using System.Collections.Generic;
using DataTier.Models;

namespace DataTier.Interfaces
{
    public interface IRepository
    {
        ExchangeRateData GetData(BusinessEntity entity);
        Dictionary<string, string> GetAvailableCurrencies();
    }
}
