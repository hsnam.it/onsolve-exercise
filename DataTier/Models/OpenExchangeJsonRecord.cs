﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using DataTier.Interfaces;

namespace DataTier.Models
{
    public class OpenExchangeJsonRecord : IQueriedJsonRecord
    {
        public string Disclaimer { get; set; }
        public string License { get; set; }
        public int Timestamp { get; set; }
        public string Base { get; set; }
        public Dictionary<string, double> Rates { get; set; }

        public void GetDataFromResponseBody(string responseBody)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Deserialize(responseBody, typeof(OpenExchangeJsonRecord));
            Disclaimer = ((OpenExchangeJsonRecord) serializedResult).Disclaimer;
            License = ((OpenExchangeJsonRecord) serializedResult).License;
            Timestamp = ((OpenExchangeJsonRecord)serializedResult).Timestamp;
            Base = ((OpenExchangeJsonRecord)serializedResult).Base;
            Rates = ((OpenExchangeJsonRecord)serializedResult).Rates;
        }
    }
}
