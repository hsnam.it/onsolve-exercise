﻿namespace DataTier.Models
{
    public class BusinessEntity
    {
        public string BaseCurrency { get; set; }
        public string ComparedCurrency { get; set; }
        public string InputDate { get; set; }
    }
}
