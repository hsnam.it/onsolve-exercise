﻿namespace DataTier.Models
{
    public class ExchangeRateData
    {
        public string BaseCurrency { get; private set; }
        public string ComparedCurrency { get; private set; }
        public string InputDate { get; set; }
        public double ExchangeRate { get; set; }

        private ExchangeRateData(){}

        public ExchangeRateData(string baseCurrency, string comparedCurrency, double exchangeRate)
        {
            this.BaseCurrency = baseCurrency;
            this.ComparedCurrency = comparedCurrency;
            this.ExchangeRate = exchangeRate;
        }
    }
}
