﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessTier;
using DataTier.Models;
using NUnit.Framework;

namespace IntegrationTest
{
    [TestFixture]
    class BusinessTierIntegrationTest
    {
        [Test]
        public void Test_Calculate_PERate_By_Regression_USD_VND_20181224()
        {
            BusinessEntity businessEntity = new BusinessEntity
            {
                InputDate = "2018-12-24",
                BaseCurrency = "USD",
                ComparedCurrency = "VND"
            };

            BusinessLogicLayer businessLogicLayer = new BusinessLogicLayer();
            double result = businessLogicLayer.CalculateExchangeRate(businessEntity);
            Assert.AreEqual(Math.Round(22730.0759782179000, 8, MidpointRounding.AwayFromZero), Math.Round(result, 8, MidpointRounding.AwayFromZero));
        }

        [Test]
        public void Test_Calculate_PERate_By_Regression_VND_USD_20181224()
        {
            BusinessEntity businessEntity = new BusinessEntity
            {
                InputDate = "2018-12-24",
                BaseCurrency = "VND",
                ComparedCurrency = "USD"
            };

            BusinessLogicLayer businessLogicLayer = new BusinessLogicLayer();

            double result = businessLogicLayer.CalculateExchangeRate(businessEntity);
            Assert.AreEqual(Math.Round(0.000043994175946, 8, MidpointRounding.AwayFromZero), Math.Round(result, 8, MidpointRounding.AwayFromZero));
        }

        [Test]
        public void Test_GetAvailableCurrencies_20181227()
        {
            BusinessLogicLayer businessLogicLayer = new BusinessLogicLayer();

            Dictionary<string, string> result = businessLogicLayer.GetAvailableCurrenciesOpenExchange();
            Assert.AreEqual(171, result.Count);
        }
    }
}
