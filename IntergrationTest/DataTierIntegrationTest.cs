﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessTier;
using DataTier;
using DataTier.Models;
using NUnit.Framework;

namespace IntegrationTest
{
    [TestFixture]
    public class DataTierIntegrationTest
    {
        [Test]
        public void Test_GetData_20181224_USD_VND()
        {
            DbContext dbContext = new DbContext();
            BusinessEntity entity = new BusinessEntity();
            entity.BaseCurrency = "USD";
            entity.ComparedCurrency = "VND";
            entity.InputDate = "2018-12-24";

            Assert.AreEqual(23273.5508, dbContext.GetOpenExchangeRepo().GetData(entity).ExchangeRate);
        }

        [Test]
        public void Test_GetAvailableCurrencies_20181227()
        {
            DbContext dbContext = new DbContext();
            Assert.AreEqual(171, dbContext.GetOpenExchangeRepo().GetAvailableCurrencies().Count);
        }
    }
}
